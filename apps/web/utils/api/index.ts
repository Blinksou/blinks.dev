export async function fetchAPI(path: string) {
  const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}${path}`);

  return await response.json();
}
