import Head from "next/head";
import React, { useContext } from "react";
import { GlobalContext } from "../_app";
import { GetServerSideProps, NextPage } from "next";
import { fetchAPI } from "../../utils/api";
import { Global, Post } from "payload-types";
import invariant from "tiny-invariant";

interface PageProps {
  post: Post;
}

const GetPost: NextPage<PageProps> = ({ post }) => {
  const global = useContext<Global>(GlobalContext);

  return (
    <>
      <Head>
        <title>
          {post.title} - {global.websiteTitle}
        </title>
        <meta property={"og:title"} content={post.seo.title} />
        <meta name={"description"} content={post.seo.description} />
        <meta property={"og:image"} content={post.seo.description} />
      </Head>
      <div>{post.title}</div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  invariant(context.params.id);

  const post: Post = await fetchAPI(`/posts/${context.params.id}`);

  if (!post.id) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {
      post,
    },
  };
};

export default GetPost;
