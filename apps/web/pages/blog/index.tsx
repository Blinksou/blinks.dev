import Head from "next/head";
import React, { useContext } from "react";
import { GlobalContext } from "../_app";
import { GetServerSideProps, NextPage } from "next";
import { fetchAPI } from "../../utils/api";
import { Global, PayloadResult, Post } from "payload-types";
import Link from "next/link";

interface PageProps {
  posts: Array<Post>;
}

const Blog: NextPage<PageProps> = ({ posts }) => {
  const global = useContext<Global>(GlobalContext);

  return (
    <>
      <Head>
        <title>Blog - {global.websiteTitle}</title>
      </Head>
      <div>Blog !!</div>
      {posts.map((post) => (
        <Link key={post.id} href={`/posts/${post.id}`}>
          <a>{post.title}</a>
        </Link>
      ))}
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const data: PayloadResult = await fetchAPI("/posts");

  const posts: Array<Post> = data.docs;

  return {
    props: {
      posts,
    },
  };
};

export default Blog;
