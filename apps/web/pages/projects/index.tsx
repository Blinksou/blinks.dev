import Head from "next/head";
import React, { useContext } from "react";
import { GlobalContext } from "../_app";
import { GetServerSideProps, NextPage } from "next";
import { fetchAPI } from "../../utils/api";
import { Global, PayloadResult, Project } from "payload-types";
import Link from "next/link";

interface PageProps {
  projects: Array<Project>;
}

const Projects: NextPage<PageProps> = ({ projects }) => {
  const global = useContext<Global>(GlobalContext);

  return (
    <>
      <Head>
        <title>Projects - {global.websiteTitle}</title>
      </Head>
      <div>Projects !!</div>
      {projects.map((project) => (
        <Link key={project.id} href={`/projects/${project.id}`}>
          <a>{project.title}</a>
        </Link>
      ))}
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const data: PayloadResult = await fetchAPI("/projects");

  const projects: Array<Project> = data.docs;

  return {
    props: {
      projects,
    },
  };
};

export default Projects;
