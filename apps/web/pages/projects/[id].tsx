import Head from "next/head";
import React, { useContext } from "react";
import { GlobalContext } from "../_app";
import { GetServerSideProps, NextPage } from "next";
import { fetchAPI } from "../../utils/api";
import { Global, Project } from "payload-types";
import invariant from "tiny-invariant";

interface PageProps {
  project: Project;
}

const GetProject: NextPage<PageProps> = ({ project }) => {
  const global = useContext<Global>(GlobalContext);

  return (
    <>
      <Head>
        <title>
          {project.title} - {global.websiteTitle}
        </title>
        <meta property={"og:title"} content={project.seo.title} />
        <meta name={"description"} content={project.seo.description} />
        <meta property={"og:image"} content={project.seo.description} />
      </Head>
      <div>{project.title}</div>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  invariant(context.params.id);

  const project: Project = await fetchAPI(`/projects/${context.params.id}`);

  if (!project.id) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: {
      project,
    },
  };
};

export default GetProject;
