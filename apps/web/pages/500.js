function SystemError() {
  return (
    <>
      <h1>Oups, une erreur est survenue</h1>
    </>
  );
}

export default SystemError;
