import "global.css";
import type { AppContext, AppProps } from "next/app";
import App from "next/app";
import Head from "next/head";
import React from "react";
import { Global } from "payload-types";
import { fetchAPI } from "../utils/api";
import Navbar from "../components/Layout/Navbar";
import Footer from "../components/Layout/Footer";

export const GlobalContext = React.createContext<Global | null>(null);

function WebApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        {pageProps.global && pageProps.global.favicon && (
          <link rel="shortcut icon" href={pageProps.global.favicon.url} />
        )}
        <title>{pageProps.global?.websiteTitle}</title>
      </Head>
      <GlobalContext.Provider value={pageProps.global ?? {}}>
        <Navbar />
        <main className={"px-2.5 md:px-5"}>
          <Component {...pageProps} />
        </main>
        <Footer />
      </GlobalContext.Provider>
    </>
  );
}

WebApp.getInitialProps = async (ctx: AppContext) => {
  const appProps = await App.getInitialProps(ctx);

  const global = await fetchAPI("/globals/global");

  return { ...appProps, pageProps: { global } };
};

export default WebApp;
