import Head from "next/head";
import React, { useContext } from "react";
import { GlobalContext } from "../_app";
import { NextPage } from "next";
import { Global } from "payload-types";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { ContactFormProps, validationSchema } from "./form";
import Button from "../../components/Common/Button";
import Input from "../../components/Common/Input";
import Label from "../../components/Common/Label";
import FormGroup from "../../components/Common/FormGroup";
import Textarea from "../../components/Common/Textarea";

interface PageProps {}

const ContactMe: NextPage<PageProps> = () => {
  const global = useContext<Global>(GlobalContext);

  const formOptions = { resolver: yupResolver(validationSchema) };

  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  const onSubmit = (data: ContactFormProps) => {
    // display form data on success
    alert("SUCCESS!! :-)\n\n" + JSON.stringify(data, null, 4));
    return false;
  };

  return (
    <>
      <Head>
        <title>Me contacter - {global.websiteTitle}</title>
      </Head>
      <div>Contact</div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormGroup error={errors.email?.message}>
          <Label htmlFor={"email"}>{`Email`}</Label>
          <Input
            {...register("email")}
            id={"email"}
            name={"email"}
            type={"text"}
            error={errors.email?.message}
          />
        </FormGroup>

        <FormGroup error={errors.subject?.message}>
          <Label htmlFor={"subject"}>{`Raison du contact`}</Label>
          <Input
            {...register("subject")}
            id={"subject"}
            name={"subject"}
            type={"text"}
            error={errors.subject?.message}
          />
        </FormGroup>

        <FormGroup error={errors.content?.message}>
          <Label htmlFor={"content"}>{`Message`}</Label>
          <Textarea
            {...register("content")}
            id={"content"}
            name={"content"}
            error={errors.content?.message}
          />
        </FormGroup>

        <FormGroup error={errors.acceptDataTreatment?.message}>
          <div className={"flex items-center"}>
            <input
              className={
                "w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
              }
              {...register("acceptDataTreatment")}
              id={"acceptDataTreatment"}
              name={"acceptDataTreatment"}
              type={"checkbox"}
            />
            <Label
              spacing={"false"}
              className={"ml-2 mb-0"}
              htmlFor={"acceptDataTreatment"}
            >
              {`J'accepte que l'on traite mes données lors de l'envoi de ce formulaire`}
            </Label>
          </div>
        </FormGroup>

        <div>
          <Button
            className={"mr-3"}
            size={"medium"}
            type={"submit"}
            variant={"primary"}
          >
            {`Envoyer`}
          </Button>
          <Button type={"button"} size={"medium"} onClick={() => reset()}>
            {`Réinitialiser`}
          </Button>
        </div>
      </form>
    </>
  );
};

export default ContactMe;
