import * as Yup from "yup";

export interface ContactFormProps {
  email: string;
  subject: string;
  content: string;
  acceptDataTreatment: boolean;
}

export const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required("Le champ email est requis")
    .email("L'email est invalide"),
  subject: Yup.string().required("Le champ sujet est requis"),
  content: Yup.string().required("Le champ message est requis"),
  acceptDataTreatment: Yup.bool().oneOf(
    [true],
    "Vous devez accepter le traitement de vos données"
  ),
});
