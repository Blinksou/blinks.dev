import { GetServerSideProps, NextPage } from "next";
import { PayloadResult, Post, Project } from "payload-types";
import { fetchAPI } from "../utils/api";

interface PageProps {
  posts: Array<Post>;
  projects: Array<Project>;
}

const Home: NextPage<PageProps> = ({ posts, projects }) => {
  return (
    <>
      <h1>Web</h1>

      <section>
        {posts.map((post) => (
          <div key={post.id}>{post.title}</div>
        ))}
      </section>

      <section>
        {projects.map((project) => (
          <div key={project.id}>{project.title}</div>
        ))}
      </section>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const [dataPost, dataProjects]: Array<PayloadResult> = await Promise.all([
    fetchAPI("/posts"),
    fetchAPI("/projects"),
  ]);

  const posts: Array<Post> = dataPost.docs;
  const projects: Array<Project> = dataProjects.docs;

  return {
    props: {
      posts,
      projects,
    },
  };
};

export default Home;
