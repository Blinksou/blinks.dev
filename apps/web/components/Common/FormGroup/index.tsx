// @ts-nocheck
import tw from "tailwind-styled-components";
import React from "react";
import FormError from "../FormError";

interface Props {
  size?: "full" | "half";
  error?: string;
  children?: React.ReactNode;
}

const FormGroupStyled = tw.div<Props>`
    ${(p) =>
      p.error
        ? "text-red-600 dark:text-red-500"
        : "text-gray-900 dark:text-gray-300"}
    
    ${(p) => (p.size === "full" || !p.size ? "w-full" : "w-1/2")}
   
    relative 
    z-0 
    mb-6 
    group
`;

const FormGroup: React.FC<Props> = ({ size, error, children }) => {
  return (
    <FormGroupStyled error={error} size={size}>
      {children}
      <FormError error={error} />
    </FormGroupStyled>
  );
};

export default FormGroup;
