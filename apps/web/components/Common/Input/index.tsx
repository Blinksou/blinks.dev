import tw from "tailwind-styled-components";

interface InputProps {
  error?: string;
}

const Input = tw.input<InputProps>`
  bg-gray-50 
  border 
  border-gray-300 
  block 
  w-full 
  p-2.5
  text-sm
  rounded-lg 
  ${(p) =>
    p.error
      ? "bg-red-50 border-red-500 text-red-900 placeholder-red-700 focus:ring-red-500  focus:border-red-500 dark:bg-red-100  dark:border-red-400"
      : "text-gray-900  focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"}
`;

export default Input;
