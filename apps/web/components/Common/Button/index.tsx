import tw from "tailwind-styled-components";

interface Props {
  variant?: "primary" | "secondary";
  size?: "normal" | "medium" | "large";
}

const Button = tw.button<Props>`
    ${(p) =>
      p.variant === "primary" ? "bg-indigo-600 text-primary" : "bg-indigo-300"}
    ${(p) =>
      p.size === "normal" || !p.size
        ? "px-4 py-2"
        : p.size === "medium"
        ? "px-5 py-2"
        : "px-5 py-3"}
    flex
    inline-flex
    items-center
    border
    border-transparent
    text-xs
    font-medium
    rounded
    shadow-sm
    text-white

    hover:bg-indigo-700
    focus:outline-none
    
    transition
    duration-150
`;

export default Button;
