import tw from "tailwind-styled-components";

interface InputProps {
  error?: string;
}

const Textarea = tw.textarea<InputProps>`
block 
p-2.5 
w-full 
text-sm 
rounded-lg border
  ${(p) =>
    p.error
      ? "bg-red-50 border-red-500 text-red-900 placeholder-red-700 focus:ring-red-500  focus:border-red-500 dark:bg-red-100  dark:border-red-400"
      : "bg-gray-50 border-gray-300 text-gray-900  focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"}
`;

export default Textarea;
