import tw from "tailwind-styled-components";

interface Props {
  spacing?: "true" | "false";
}

const Label = tw.label<Props>`
  ${(p) => (p.spacing === "true" || !p.spacing ? "mb-2" : "mb-0")}
  
  block 
  text-sm 
  font-medium 
`;

export default Label;
