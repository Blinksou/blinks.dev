import tw from "tailwind-styled-components";
import React from "react";

const FormErrorStyled = tw.span`
    mt-2 text-sm text-red-600 dark:text-red-500
`;

interface Props {
  error?: string;
}

const FormError: React.FC<Props> = ({ error }) => {
  return error ? <FormErrorStyled>{error}</FormErrorStyled> : null;
};

export default FormError;
