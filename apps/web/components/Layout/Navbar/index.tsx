import Link from "next/link";
import React from "react";

interface Props {}

const Navbar: React.FC<Props> = () => {
  return (
    <>
      <nav>
        <Link href="/">
          <a>Home</a>
        </Link>
        <Link href="/blog">
          <a>Blog</a>
        </Link>
        <Link href="/projects">
          <a>Projects</a>
        </Link>
      </nav>
    </>
  );
};

export default Navbar;
