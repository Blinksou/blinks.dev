import { CollectionConfig } from "payload/types";
import SeoField from "../fields/groups/Seo";

const Projects: CollectionConfig = {
  slug: "projects",
  admin: {
    // defaultColumns: ['title', 'author', 'category', 'tags', 'status'],
    useAsTitle: "title",
  },
  access: {
    read: () => true,
  },
  fields: [
    {
      name: "title",
      type: "text",
      required: true,
    },
    {
      name: "link",
      type: "text",
    },
    {
      name: "thumbnail",
      type: "upload",
      relationTo: "media",
    },
    {
      name: "content",
      type: "richText",
    },
    SeoField,
  ],
};

export default Projects;
