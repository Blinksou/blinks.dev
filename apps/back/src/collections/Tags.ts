import { CollectionConfig } from "payload/types";
import colorPicker from "../fields/fields/ColorPicker";

const Tags: CollectionConfig = {
  slug: "tags",
  admin: {
    useAsTitle: "name",
  },
  access: {
    read: () => true,
  },
  fields: [
    {
      name: "name",
      type: "text",
    },
    colorPicker,
  ],
  timestamps: false,
};

export default Tags;
