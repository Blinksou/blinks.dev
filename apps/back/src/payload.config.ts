import { buildConfig } from "payload/config";
import path from "path";
import Categories from "./collections/Categories";
import Posts from "./collections/Posts";
import Tags from "./collections/Tags";
import Users from "./collections/Users";
import Global from "./globals/Global";
import Media from "./collections/Media";
import Projects from "./collections/Projects";

export default buildConfig({
  serverURL: "http://localhost:3000",
  admin: {
    user: Users.slug,
  },
  collections: [Categories, Posts, Tags, Users, Media, Projects],
  globals: [Global],
  typescript: {
    outputFile: path.resolve(
      __dirname,
      "../../../packages/payload-types/payload-types.ts"
    ),
  },
});
