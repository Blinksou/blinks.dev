import { Field } from "payload/types";

const SeoField: Field = {
  name: "seo",
  type: "group",
  fields: [
    {
      name: "title",
      type: "text",
      required: true,
      maxLength: 60,
      admin: {
        description: ({ value }) =>
          `${
            typeof value === "string" ? 60 - value.length : "60"
          } characters left`,
      },
    },
    {
      name: "description",
      type: "text",
      maxLength: 150,
      admin: {
        description: ({ value }) =>
          `${
            typeof value === "string" ? 150 - value.length : "150"
          } characters left`,
      },
    },
    {
      name: "image",
      type: "upload",
      relationTo: "media",
    },
  ],
};

export default SeoField;
