import { GlobalConfig } from "payload/types";

const Global: GlobalConfig = {
  slug: "global",
  access: {
    read: () => true,
  },
  fields: [
    {
      name: "websiteTitle",
      type: "text",
    },
    {
      name: "favicon",
      type: "upload",
      relationTo: "media",
    },
  ],
};

export default Global;
