import { Media } from "./payload-types";

export * from "./payload-types";

export interface PayloadResult {
  docs: Array<any>;
  totalDocs: number;
  limit: number;
  totalPages: number;
  pages: number;
  pagingCounter: number;
  hasPrevPage: boolean;
  hasNextPage: boolean;
  prevPage: number | null;
  nextPage: number | null;
}

export interface SeoGroupFields {
  title: string;
  description?: string;
  image?: string | Media;
}
